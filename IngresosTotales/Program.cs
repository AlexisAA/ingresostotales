﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using Newtonsoft.Json.Serialization;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;

namespace IngresosTotales
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                Datos(args[0].ToString());
            }
            else
            {
                Datos("");
            }
            Thread.Sleep(1000);
        }
        public static void Datos(string dates)
        {
            string fechain = "", fechafn = "", respuesta1="", estacionamiento ="",fechacon = "",co = "";
            int co1 = 0;
            decimal ing = 0;
            List<string> lista = new List<string>();
            DateTime fechainicio = DateTime.Now;
            String fecha1 = DateTime.Today.AddDays(-1).ToString("yyyy/MM/dd");
                if (dates.Length <= 0)
                {
                    fechain = DateTime.Today.AddDays(-1).ToString("dd/MM/yyyy 00:00");
                    fechafn = DateTime.Today.AddDays(-1).ToString("dd/MM/yyyy 23:59");
                    fechacon = DateTime.Today.AddDays(-1).ToString("yyyy/MM/dd");
                //consulta
                string conex = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionSQL"].ConnectionString;
                SqlConnection conexion2 = new SqlConnection(conex);
                conexion2.Open();
                String conec1 = "select * from TBL_Ingresoestacionamientos where fecha = '" + fechacon + "' and id_empresa = 2";
                SqlCommand comand = new SqlCommand(conec1, conexion2);
                SqlDataReader registros = comand.ExecuteReader();
                while (registros.Read())
                {
                    co = registros[0].ToString();
                    lista.Add(co);
                    co1 = Int32.Parse(co);
                }
                conexion2.Close();
                if (co1==0)
                {
                    Tickets.wsParkingExportSoapClient fade = new Tickets.wsParkingExportSoapClient();
                    Tickets.getGlobalCardInfoRequest envio = new Tickets.getGlobalCardInfoRequest();
                    Tickets.getGlobalCardInfoResponse respuesta = new Tickets.getGlobalCardInfoResponse();
                    try
                    {
                        respuesta1 = fade.getGlobalCardInfo(fechain, fechafn);
                        try
                        {
                            List<Respuesta> obj = JsonConvert.DeserializeObject<List<Respuesta>>(respuesta1);
                            for (int i = 0; i < obj.Count; i++)
                            {
                                if (obj[i].Estatus == "Pagado" && obj[i].importe != "0.0000" || obj[i].Estatus == "Perdido" && obj[i].importe != "0.0000")
                                {
                                    estacionamiento = obj[i].idestacionamiento;
                                    ing += Convert.ToDecimal(obj[i].importe);
                                }
                            }
                            string proc = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionSQL"].ConnectionString;
                            SqlConnection conexion1 = new SqlConnection(proc);
                            conexion1.Open();
                            string con = "insert into TBL_Ingresoestacionamientos (id_sistema,Ingreso,fecha, id_empresa) values (" + estacionamiento + "," + ing + ",'" + fecha1 + "',2)";
                            SqlCommand Comando = new SqlCommand(con, conexion1);
                            Comando.ExecuteNonQuery();
                            conexion1.Close();
                            Console.WriteLine("Acayá Mazatlán: " + ing);
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Acayá Mazatlán No se encontro ningun registro");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Acayá Mazatlán Conexión Perdida " + ex);

                    }
                   
                }
                else
                {
                    Tickets.wsParkingExportSoapClient fade = new Tickets.wsParkingExportSoapClient();
                    Tickets.getGlobalCardInfoRequest envio = new Tickets.getGlobalCardInfoRequest();
                    Tickets.getGlobalCardInfoResponse respuesta = new Tickets.getGlobalCardInfoResponse();
                    try
                    {
                        respuesta1 = fade.getGlobalCardInfo(fechain, fechafn);
                        try
                        {
                            List<Respuesta> obj = JsonConvert.DeserializeObject<List<Respuesta>>(respuesta1);
                            for (int i = 0; i < obj.Count; i++)
                            {
                                if (obj[i].Estatus == "Pagado" && obj[i].importe != "0.0000" || obj[i].Estatus == "Perdido" && obj[i].importe != "0.0000")
                                {
                                    estacionamiento = obj[i].idestacionamiento;
                                    ing += Convert.ToDecimal(obj[i].importe);
                                }
                            }
                            string proc = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionSQL"].ConnectionString;
                            SqlConnection conexion1 = new SqlConnection(proc);
                            conexion1.Open();
                            string con = "update TBL_Ingresoestacionamientos set Ingreso = " + Convert.ToDecimal(ing) + " where fecha = '" + fecha1 + "' and id_empresa = 2"; ;
                            SqlCommand Comando = new SqlCommand(con, conexion1);
                            Comando.ExecuteNonQuery();
                            conexion1.Close();
                            Console.WriteLine("Acayá Mazatlán Actualizado " );
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Acayá Mazatlán No se encontro ningun registro");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Acayá Mazatlán Conexión Perdida " + ex);

                    }
                    
                }
                    //FIN SIN FECHA
                }
                else
                {
                    fechainicio = Convert.ToDateTime(dates);
                    
                    string fechainicio1 = Convert.ToString(fechainicio.ToString("dd/MM/yyyy 00:00"));
                    string fechafin = Convert.ToString(fechainicio.ToString("dd/MM/yyyy 23:59"));
                //consulta
                string conex = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionSQL"].ConnectionString;
                SqlConnection conexion2 = new SqlConnection(conex);
                conexion2.Open();
                String conec1 = "select * from TBL_Ingresoestacionamientos where fecha = '" + fechainicio.ToString("yyyy/MM/dd") + "' and id_empresa = 2";
                SqlCommand comand = new SqlCommand(conec1, conexion2);
                SqlDataReader registros = comand.ExecuteReader();
                while (registros.Read())
                {
                    co = registros[0].ToString();
                    lista.Add(co);
                    co1 = Int32.Parse(co);
                }
                conexion2.Close();
                if (co1==0)
                {
                    Tickets.wsParkingExportSoapClient fade = new Tickets.wsParkingExportSoapClient();
                    Tickets.getGlobalCardInfoRequest envio = new Tickets.getGlobalCardInfoRequest();
                    Tickets.getGlobalCardInfoResponse respuesta = new Tickets.getGlobalCardInfoResponse();

                    try
                    {
                        respuesta1 = fade.getGlobalCardInfo(fechainicio1, fechafin);
                        try
                        {
                            List<Respuesta> obj = JsonConvert.DeserializeObject<List<Respuesta>>(respuesta1);
                            for (int i = 0; i < obj.Count; i++)
                            {
                                if (obj[i].Estatus == "Pagado" && obj[i].importe != "0.0000" || obj[i].Estatus == "Perdido" && obj[i].importe != "0.0000")
                                {
                                    estacionamiento = obj[i].idestacionamiento;
                                    ing += Convert.ToDecimal(obj[i].importe);
                                }
                            }
                            string proc = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionSQL"].ConnectionString;
                            SqlConnection conexion1 = new SqlConnection(proc);
                            conexion1.Open();
                            string con = "insert into TBL_Ingresoestacionamientos (id_sistema,Ingreso,fecha, id_empresa) values (" + estacionamiento + "," + ing + ",'" + fechainicio.ToString("yyyy/MM/dd") + "',2)";
                            SqlCommand Comando = new SqlCommand(con, conexion1);
                            Comando.ExecuteNonQuery();
                            conexion1.Close();
                            Console.WriteLine("Acayá Mazatlán: " + ing);
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Acayá Mazatlán No se encontro ningun registro");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Acayá Mazatlán Conexión Perdida " + ex);

                    }
                   

                }
                else
                {
                    Tickets.wsParkingExportSoapClient fade = new Tickets.wsParkingExportSoapClient();
                    Tickets.getGlobalCardInfoRequest envio = new Tickets.getGlobalCardInfoRequest();
                    Tickets.getGlobalCardInfoResponse respuesta = new Tickets.getGlobalCardInfoResponse();

                    try
                    {
                        respuesta1 = fade.getGlobalCardInfo(fechainicio1, fechafin);
                        try
                        {
                            List<Respuesta> obj = JsonConvert.DeserializeObject<List<Respuesta>>(respuesta1);
                            for (int i = 0; i < obj.Count; i++)
                            {
                                if (obj[i].Estatus == "Pagado" && obj[i].importe != "0.0000" || obj[i].Estatus == "Perdido" && obj[i].importe != "0.0000")
                                {
                                    estacionamiento = obj[i].idestacionamiento;
                                    ing += Convert.ToDecimal(obj[i].importe);
                                }
                            }
                            string proc = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionSQL"].ConnectionString;
                            SqlConnection conexion1 = new SqlConnection(proc);
                            conexion1.Open();
                            string con = "update TBL_Ingresoestacionamientos set Ingreso = " + ing + " where fecha = '" + fechainicio.ToString("yyyy/MM/dd") + "' and id_empresa = 2";
                            SqlCommand Comando = new SqlCommand(con, conexion1);
                            Comando.ExecuteNonQuery();
                            conexion1.Close();
                            Console.WriteLine("Acayá Mazatlán Actualizado");
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Acayá Mazatlán No se encontro ningun registro");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Acayá Mazatlán Conexión Perdida " + ex);

                    }
                    
                }
                
            }
        }
        

        public class Respuesta
        {
            public string idestacionamiento { get; set; }
            public string ticket_num { get; set; }
            public string Estatus { get; set; }
            public string fechaEntrada { get; set; }
            public string fecha { get; set; }
            public string importe { get; set; }
        }
        public class Respuesta2
        {
            public Respuesta[] Property1 { get; set; }        }
    }
}
